#include <iostream>
#include <string>

#include "utf8.h"

int main() {
    auto message = std::string{"test message"};
    auto message_u16 = utf8::utf8to16(message);

    (void)message_u16;

    return 0;
}
