import os
import shutil

from conans import ConanFile, tools


class UtfcppConan(ConanFile):
    name = "utfcpp"
    version = "3.1.2"
    license = "BSL-1.0"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-utfcpp/"
    description = "UTF-8 with C++ in a Portable Way "
    topics = ("UTF-8", "header-only")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        zipfile = "utfcpp.zip"
        tools.download("https://github.com/nemtrif/utfcpp/archive/v{}.zip".format(self.version), zipfile)
        tools.unzip(zipfile)
        shutil.move("utfcpp-{}".format(self.version), "utfcpp")
        os.unlink(zipfile)

    def package(self):
        self.copy("*.h", "include", src="utfcpp/source")
